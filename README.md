# Kabyle (Taqbaylit) latin script using Unicode

Basic latin block U+0000 (Range: 0000–007F) except the vowel/letter small o and capital O :

## Unicode table :

By adding the following Characters to the Basic Unicode block, you will get *nearly* a full Kabyle latin Unicode.

| Unicode | small letter |Description| Unicode | CAPITAL letter |Description|
|:--:|:------:|:------:| :------: | :------: |:------:|
| U+010D | č |LATIN SMALL LETTER C WITH CARON| U+010C | Č |LATIN CAPITAL LETTER C WITH CARON|
| U+1E0D | ḍ |LATIN SMALL LETTER D WITH DOT BELOW |U+1E0C|Ḍ|LATIN CAPITAL LETTER D WITH DOT BELOW|
| U+01E7 | ǧ |LATIN SMALL LETTER G WITH CARON|U+01E6|Ǧ|LATIN CAPITAL LETTER G WITH CARON|
| U+1E25 | ḥ |LATIN SMALL LETTER H WITH DOT BELOW|U+1E24|Ḥ|LATIN CAPITAL LETTER H WITH DOT BELOW|
| U+1E5B | ṛ |LATIN SMALL LETTER R WITH DOT BELOW|U+1E5A|Ṛ|LATIN CAPITAL LETTER R WITH DOT BELOW|
| U+1E63 | ṣ |LATIN SMALL LETTER S WITH DOT BELOW|U+1E62|Ṣ|LATIN CAPITAL LETTER S WITH DOT BELOW|
| U+1E6D | ṭ |LATIN SMALL LETTER T WITH DOT BELOW|U+1E6C|Ṭ|LATIN CAPITAL LETTER T WITH DOT BELOW|
| U+1E93 | ẓ |LATIN SMALL LETTER Z WITH DOT BELOW|U+1E92|Ẓ|LATIN CAPITAL LETTER Z WITH DOT BELOW|
| U+03B3 | γ |GREEK SMALL LETTER GAMMA| U+0393 | Γ |GREEK CAPITAL LETTER GAMMA|
| U+025B | ɛ |LATIN SMALL LETTER OPEN E| U+0190 | Ɛ |LATIN CAPITAL LETTER OPEN E|
| U+02B7 | ʷ |MODIFIER LETTER SMALL W| U+02B7 | ʷ |MODIFIER LETTER SMALL W|

- Kabyle (Taqbaylit) latin script uses **U+0393 Greek capital letter gamma** "**Γ**" instead of Latin capital gama below.

| Unicode | small letter |Description| Unicode | CAPITAL letter |Description|
|:--:|:------:|:------:| :------: | :------: |:------:|
|U+0263|ɣ|LATIN SMALL LETTER GAMMA|U+0194|Ɣ|LATIN CAPITAL LETTER GAMMA |

# xKeyboard for X11 :

dz keyboard layout, work in progress here

See : https://gitlab.com/BoFFire/xkb_dz
